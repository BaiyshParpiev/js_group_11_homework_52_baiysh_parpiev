import './App.css';
import './components/cards.css'
import CardDeck from "./components/CardDeck";
import {Component} from 'react';

class App extends Component {
    state = {
        isShow: false,
        cardDeck: new CardDeck(),
        firstFiveCards: 0,
        countChangeCards: 0,
        newCards: 0,
        toOpenNewCard: 0,
        cards: [],
        winning: [],
    }
    newCards = () => {
     if(this.state.toOpenNewCard < 4){
         const newCards = this.state.cardDeck.getCards(this.state.toOpenNewCard);
         this.setState({
             cards: this.state.cards.map((p) =>{
                 for(let i = 0; i < newCards.length; i++){
                     return {p: newCards}
                 }
             })
         })
     }
    }
    getOutcome = () => {
        if(this.state.cards.length > 0){
            const suits = {
                hearts: 0,
                clubs: 0,
                spades: 0,
                diams: 0,
            }
            const ranks = {
                2 : 0,
                3 : 0,
                4 : 0,
                5 : 0,
                6 : 0,
                7 : 0,
                8 : 0,
                9 : 0,
                10: 0,
                J : 0,
                Q : 0,
                K : 0,
                A : 0,
            }


            const point = [{double: 0}, {trouble: 0},{twoCards: 0}, {flash: 0}, {fullHouse: 0}, {poker:0}]

            this.state.cards.map(card => {
                for(const key in suits){
                    if(card.suitName === key){
                        suits[key]++
                    }
                    continue;
                }

                for(const key in ranks){
                    if(!isNaN(card.ranks)){
                        if(card.ranks === parseInt(key)){
                            ranks[key]++
                        }
                    }else if(isNaN(card.ranks)){
                        if(card.ranks === key){
                            ranks[key]++
                        }
                    }
                    continue;
                }

            });
            for(const key in ranks){
                if(ranks[key] > 3){
                    point[5].poker++
                } else if(ranks[key] > 2){
                    ++point[1].trouble
                } else if(ranks[key] > 1){
                    ++point[0].double;
                }
            }
            for(let key in suits){
                if(suits[key] > 4){
                    point[3].flash++
                }
            }
            if(point[0].double > 0 && point[1].trouble > 0){
                point[4].fullHouse++
            }else if(point[0].double > 1){
                point[2].twoCards++;
            }

            const indexOfWinning = [];
            for(let i = 0; i < point.length; i++){
                for(const key in point[i]){
                    if(point[i][key] > 0){
                        indexOfWinning.push(i)
                    }
                }
            }
            const index = Math.max.apply(null, indexOfWinning);
           if(index === 0){
                this.setState({winning: 2})
            }else if(index === 1){
               this.setState({winning: 4})
            }else if(index === 2){
               this.setState({winning: 6})
            }else if(index === 3){
               this.setState({winning: 8})
            }else if(index === 4){
                this.setState({winning: 16})
            }else if(index === 5){
                this.setState({winning: 20})
            }else if(!isNaN(index)){
               this.setState({winning: 0})
            }
        }
    }
    handleToggle = () => {
        if(this.state.toOpenNewCard < 1 && this.state.firstFiveCards < 1){
            this.state.cardDeck.makeCards();
            this.setState({cards: this.state.cardDeck.getCards(5)})
            const {isShow} = this.state;
            this.setState({isShow: !isShow});
            this.state.countChangeCards += 5;
            this.state.newCards += 1;
            ++this.state.firstFiveCards;
            this.getOutcome()
        } else if((this.state.newCards > 0 && this.state.newCards < 2) && this.state.toOpenNewCard > 0){
            this.newCards()
        }else if(this.state.newCards > 1){
            alert('You can only change max 3 of cards in  one time')
        }
    }
    targetToggle = (e) => {
        if(e.currentTarget.className !== 'card back'){
            if(this.state.toOpenNewCard > 3){
                alert('You can  change max 3 cards')
            }else if(this.state.toOpenNewCard < 3){
                this.state.toOpenNewCard++;
                return e.currentTarget.className = 'card back'
            }
        }
    }
    type =()=>{
        const {isShow} = this.state;
        this.setState({isShow: !isShow});
    }

    render() {
        const Card = props => {
            return (
                <li>
                    <div onClick={this.targetToggle.bind(this)} className={this.state.isShow ? `card rank-${props.ranks.toString().toLowerCase()} ${props.suitName}`: 'card back'}>
                        <span className="rank">{this.state.isShow? props.ranks : ""}</span>
                        <span className="suit">{this.state.isShow? props.suit : ""}</span>
                    </div>
                </li>
            )
        };

        const changedCards = this.state.cards.map((card) => (
            <Card key = {card.id} suit={card.suit} ranks={card.ranks} suitName={card.suitName}/>
        ));

        return (
            <div className="playingCards">
                <div className="container">
                    <table>
                        <tbody>
                            <tr>
                                <td>Royal Flush</td>
                                <td>250</td>
                                <td>500</td>
                                <td>750</td>
                                <td>1000</td>
                                <td>5000</td>
                            </tr>
                            <tr>
                                <td>Straight Flush</td>
                                <td>50</td>
                                <td>100</td>
                                <td>150</td>
                                <td>200</td>
                                <td>250</td>
                            </tr>
                            <tr>
                                <td>Four of a Kind</td>
                                <td>25</td>
                                <td>50</td>
                                <td>75</td>
                                <td>100</td>
                                <td>125</td>
                            </tr>
                            <tr>
                                <td>Full House</td>
                                <td>9</td>
                                <td>18</td>
                                <td>27</td>
                                <td>36</td>
                                <td>45</td>
                            </tr>
                            <tr>
                                <td>Flush</td>
                                <td>6</td>
                                <td>12</td>
                                <td>18</td>
                                <td>24</td>
                                <td>30</td>
                            </tr>
                            <tr>
                                <td>Straight</td>
                                <td>4</td>
                                <td>8</td>
                                <td>12</td>
                                <td>16</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>Three of a Kind</td>
                                <td>3</td>
                                <td>6</td>
                                <td>9</td>
                                <td>12</td>
                                <td>15</td>
                            </tr>
                            <tr>
                                <td>Two Pairs</td>
                                <td>2</td>
                                <td>4</td>
                                <td>6</td>
                                <td>8</td>
                                <td>10</td>
                            </tr>
                            <tr>
                                <td>Jacks or Better</td>
                                <td>1</td>
                                <td>2</td>
                                <td>3</td>
                                <td>4</td>
                                <td>5</td>
                            </tr>
                        </tbody>
                    </table>
                    <div className="credits">
                        <ul className="table-credits">
                            <li>
                               Bite: 0
                            </li>
                            <li className="winning">
                                Win: {this.state.winning}
                            </li>
                            <li>
                                Credits: 100
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="container cards">
                    <ul className="table container">
                        {changedCards}
                    </ul>
                    <div className='container'>
                        <button onClick={this.handleToggle} className='btn-draw'>Deal Draw</button>
                        <button onClick={this.getOutcome} className='btn-max'>result</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
