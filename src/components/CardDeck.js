class CardDeck {
    constructor() {
        this.exampleCards = {
            countOfCard: 51,
            card:[],
            fiveCard: [],
        }
    }
    randomArray (count, min, max){
        if(count > (max - min)) return;
        const arr = [];
        while (count){
            const number =  Math.floor(Math.random() * (max - min + 1) + min);
            if(arr.indexOf(number) === -1){
                arr.push(number);
                count--;
            }
            --max;
        }
        return arr;
    }
    makeCards = () => {
        const cardsArray = ['H', 'D', 'C', 'S'];
        const cardsNoNumber = ['J', 'Q', 'K', 'A'];

        for(let i = 2; i < 11; i++){
            for(let j = 0; j < cardsArray.length; j++){
                const cardsObject = {suit: cardsArray[j], ranks:i, id: cardsArray[j] + i}
                this.exampleCards.card.push(cardsObject);
            }
        }

        for(let j = 0; j < cardsArray.length; j++){
            for(let i = 0; i < cardsNoNumber.length; i++){
                const cardsObject = {suit: cardsArray[j], ranks:cardsNoNumber[i], id: cardsArray[j] + cardsNoNumber[i]}
                this.exampleCards.card.push(cardsObject);
            }
        }
    };
    getCard = (number)=> {
        return this.exampleCards.card.splice(number, 1);
    }
    getCards = (howMany) => {
        const arrayNumber = this.randomArray(howMany, 0, this.exampleCards.countOfCard);
        for(let i = 0; i < howMany; i++){
            const getCard = this.getCard(arrayNumber[i]);
            if(getCard[0].suit === 'D'){
                getCard[0].suitName = 'diams'
                getCard[0].suit = '♦'
            } else if(getCard[0].suit === 'H'){
                getCard[0].suitName = 'hearts';
                getCard[0].suit = '♥';
            } else if(getCard[0].suit === 'C'){
                getCard[0].suitName = 'clubs';
                getCard[0].suit = '♣';
            } else{
                getCard[0].suitName = 'spades';
                getCard[0].suit = '♠';
            }
            this.exampleCards.fiveCard.push(getCard[0]);
        }
        return this.exampleCards.fiveCard;
    }
}


export default CardDeck;